#!/bin/bash
set -ex
# ~/opt/bbmap/resources/adapters.fa
# ~/opt/bbmap/resources/phix174_ill.ref.fa.gz \

fastq=$1
adapters=$2
phix=$2

bbduk.sh -Xmx1g in=${fastq} out=trimmed.fq interleaved overwrite bf1 \
                ktrim=r k=23 mink=11 hdist=1 ref=${adapters} \
                tbo tpe 2> bbduk.log 

bbduk.sh -Xmx1g in=trimmed.fq out=clean.fastq interleaved overwrite bf1 \
                k=27 hdist=1 qtrim=rl trimq=17 cardinality=t \
                ref=${phix} 2>> bbduk.log

#bfc -1 -s 10g -k 21 -t 20 clean.fastq 2> /dev/null | seqtk dropse - > bfc_k21.fastq

spades.py -o results --meta -k 21,33,55,77,99,127 --12 clean.fastq &> spades.log

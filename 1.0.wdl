workflow jgi_meta_annot_torben {

    File fastq
    File adapters="/global/dna/shared/databases/DSI/bbtools_ref/adapters.fa"
    File phix="/global/dna/shared/databases/DSI/bbtools_ref/phix174_ill.ref.fa"

    call filterReads {
        input: fastq=fastq,
            adapters=adapters,
            phix=phix
    }
    call spades {
        input: cleaned=filterReads.cleaned
    }
}

task filterReads {
    File fastq
    File adapters
    File phix

    command {
        shifter --image=jfroula/jgi_meta_assem_torben:0.1.3 bbduk.sh -Xmx1g \
                in=${fastq} \
                out=trimmed.fq \
                interleaved overwrite bf1 \
                ktrim=r k=23 mink=11 hdist=1 ref=${adapters} \
                tbo tpe 2> bbduk.log

        shifter --image=jfroula/jgi_meta_assem_torben:0.1.3 bbduk.sh -Xmx1g \
                in=trimmed.fq out=clean.fastq interleaved overwrite bf1 \
                k=27 hdist=1 qtrim=rl trimq=17 cardinality=t \
                ref=${phix} 2>> bbduk.log
    }
    output {
        File bbduk_log = "bbduk.log"
        File cleaned = "clean.fastq"
    }
    runtime {
        time: "48:00:00"
#        time: "2:00:00"
#        mem: "115G"
        mem: "500G"
        cluster: "cori"
        poolname:  "filter"
        node: 1
        nwpn: 1
        
    }
}

task spades {
    File cleaned
    Int threads
    Int spades_max_mem=450

    command {
        shifter --image=jfroula/jgi_meta_assem_torben:0.1.3 spades.py \
                -o results --meta -k 21,33,55,77,99,127 \
                --mem=${spades_max_mem} --threads ${threads} \
                --12 ${cleaned} &> spades.log
    }
    output {
        File results = "results"
        File spades_log = "spades.log"
    }
    runtime { 
        cpu: threads
        time: "48:00:00"
        mem: "500G"
#        time: "2:00:00"
#        mem: "115G"
        cluster: "cori"
        poolname:  "filter"
        node: 1
        nwpn: 1
    }
}



FROM continuumio/miniconda
RUN conda config --add channels defaults && \
    conda config --add channels bioconda && \
    conda config --add channels conda-forge

RUN conda install -y -c bioconda seqtk==1.3
RUN conda install -y -c bioconda bfc=r181
RUN conda install -y -c bioconda spades==3.10.0
RUN conda install -y -c bioconda bbmap==38.45
COPY ref /ref
